{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Handler.PostNew where

import Import
import Yesod.Form.Bootstrap3
import Yesod.Form.Nic (YesodNic, nicHtmlField)
instance YesodNic App

blogPostForm :: AForm Handler Article
blogPostForm = Article 
             <$> areq textField (bfs ("Title" :: Text)) Nothing
             <*> areq nicHtmlField (bfs ("Content" :: Text)) Nothing

getPostNewR :: Handler Html
getPostNewR = do
  (widget, enctype) <- generateFormPost $ renderBootstrap3 BootstrapBasicForm blogPostForm
  defaultLayout $ do
    $(widgetFile "post/new")

postPostNewR :: Handler Html
postPostNewR = do
  ((res, widget), enctype) <- runFormPost $ renderBootstrap3 BootstrapBasicForm blogPostForm
  case res of 
    FormSuccess blogPost -> do
      _ <- runDB $ insert blogPost
      redirect HomeR
    _ -> defaultLayout $(widgetFile "post/new")
