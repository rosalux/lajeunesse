{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module Handler.Article where

import Import

getArticleR :: ArticleId -> Handler Html
getArticleR articleId = do
  article <- runDB $ get404 articleId
  defaultLayout $ do
    setTitle$ toHtml $ articleTitle article
    $(widgetFile "blog/article")
