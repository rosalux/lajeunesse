{-# LANGUAGE PackageImports #-}
import "lajeunesse" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
